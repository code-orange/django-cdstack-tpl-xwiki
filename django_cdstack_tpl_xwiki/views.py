import copy

import yaml
from django.template import engines

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
)
from django_cdstack_tpl_deb_bullseye.django_cdstack_tpl_deb_bullseye.views import (
    handle as handle_deb_bullseye,
)
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
)
from django_cdstack_tpl_crowdsec_main.django_cdstack_tpl_crowdsec_main.views import (
    handle as handle_crowdsec_main,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)
from django_cdstack_tpl_whclient.django_cdstack_tpl_whclient.views import (
    handle as handle_whclient,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_xwiki/django_cdstack_tpl_xwiki"

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    with open(
        module_prefix + "/templates/config-fs/dynamic/etc/pomerium/config.yaml", "r"
    ) as pomerium_config_file:
        pomerium_config = yaml.safe_load(pomerium_config_file)

        pomerium_config["cookie_secret"] = template_opts["pomerium_cookie_secret"]
        pomerium_config["shared_secret"] = template_opts["pomerium_shared_secret"]

        pomerium_config["idp_provider_url"] = template_opts["pomerium_idp_provider_url"]
        pomerium_config["idp_provider"] = template_opts["pomerium_idp_provider"]
        pomerium_config["idp_client_id"] = template_opts["pomerium_idp_client_id"]
        pomerium_config["idp_client_secret"] = template_opts[
            "pomerium_idp_client_secret"
        ]

        pomerium_config["authenticate_service_url"] = template_opts[
            "pomerium_authenticate_service_url"
        ]
        pomerium_config["forward_auth_url"] = template_opts["pomerium_forward_auth_url"]

        pomerium_config["policy"] = list()

        pomerium_config["policy"].append(
            {
                "from": "https://" + template_opts["xwiki_fqdn"],
                "to": "https://" + template_opts["xwiki_fqdn"],
                "pass_identity_headers": True,
                "allow_any_authenticated_user": True,
            }
        )

        zip_add_file(
            zipfile_handler, "/etc/pomerium/config.yaml", yaml.dump(pomerium_config)
        )

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_whclient(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_ipcop(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_fail2ban(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_crowdsec_main(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_wazuh_agent(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    if not skip_handle_os:
        handle_deb(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
