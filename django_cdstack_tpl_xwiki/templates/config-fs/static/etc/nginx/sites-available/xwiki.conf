# xwiki

server {
    listen 80 default_server;
    listen [::]:80 default_server;

    server_name _;

    location ^~ /.well-known/acme-challenge/ {
        access_log off;
        log_not_found off;

        proxy_pass {{ acme_forward_protocol }}acme;

        proxy_http_version 1.1;

        # Mitigating the HTTPoxy Vulnerability
        proxy_set_header Proxy "";

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    return 301 https://$host$request_uri;
}

server {
    listen 443 default_server ssl http2;
    listen [::]:443 default_server ssl http2;

    ssl_certificate /etc/ssl/certs/host.pem;
    ssl_certificate_key /etc/ssl/private/host.key;

    server_name _;

    location ^~ /.well-known/acme-challenge/ {
        access_log off;
        log_not_found off;

        proxy_pass {{ acme_forward_protocol }}acme;

        proxy_http_version 1.1;

        # Mitigating the HTTPoxy Vulnerability
        proxy_set_header Proxy "";

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location = /ext_authz {
        internal;

        proxy_pass_request_body off;
        proxy_set_header Content-Length "";
        proxy_set_header X-Forwarded-Proto "";

        proxy_set_header Host fwdauth.{{ xwiki_fqdn }};
        proxy_set_header X-Original-URL $scheme://$http_host$request_uri;
        proxy_set_header X-Original-Method $request_method;
        proxy_set_header X-Real-IP $remote_addr;

        proxy_set_header X-Forwarded-For $remote_addr;

        proxy_set_header X-Auth-Request-Redirect $request_uri;

        proxy_buffering off;

        proxy_buffer_size 4k;
        proxy_buffers 4 4k;
        proxy_request_buffering on;
        proxy_http_version 1.1;

        proxy_ssl_server_name on;
        proxy_pass_request_headers on;

        client_max_body_size 1m;

        # Pass the extracted client certificate to the auth provider

        set $target https://pomerium/verify?uri=$scheme://$http_host$request_uri;

        # uncomment to emulate nginx-ingress behavior
        # set $target https://pomerium/verify?uri=$scheme://$http_host$request_uri&rd=$pass_access_scheme://$http_host$escaped_request_uri;
        proxy_pass $target;
    }

    location @authredirect {
        internal;
        add_header Set-Cookie $auth_cookie;

        # uncomment to emulate nginx-ingress behavior
        # return 302 https://fwdauth.{{ xwiki_fqdn }}/?uri=$scheme://$host$request_uri&rd=$pass_access_scheme://$http_host$escaped_request_uri;

        return 302
        https://fwdauth.{{ xwiki_fqdn }}/?uri=$scheme://$host$request_uri;
    }

    location / {
        proxy_pass http://xwiki;

        include /etc/nginx/proxy.conf;
        # If we get a 401, respond with a named location
        error_page 401 = @authredirect;
        # this location requires authentication
        auth_request /ext_authz;

        auth_request_set $auth_cookie $upstream_http_set_cookie;
        auth_request_set $x_pomerium_claim_sub $upstream_http_x_pomerium_claim_sub;
        auth_request_set $x_pomerium_claim_name $upstream_http_x_pomerium_claim_name;
        auth_request_set $x_pomerium_claim_email $upstream_http_x_pomerium_claim_email;

        proxy_set_header X-Pomerium-Claim-Sub $x_pomerium_claim_sub;
        proxy_set_header X-Pomerium-Claim-Name $x_pomerium_claim_name;
        proxy_set_header X-Pomerium-Claim-Email $x_pomerium_claim_email;

        add_header Set-Cookie $auth_cookie;
    }
}
